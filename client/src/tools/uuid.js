export default class UUID {
  static uuid4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
      const r = Math.random() * 16 | 0
      const v = c == 'x' ? r : (r & 0x3 | 0x8)
      return v.toString(16)
    })
  }

  static getOrCreate() {
    let uuid = localStorage.getItem('UUID')
    if (!uuid) {
      uuid = this.uuid4()
      localStorage.setItem('UUID', uuid)
    }
    return uuid
  }
}
