const path = require('path')
const colors = require('vuetify/es5/util/colors').default

const ROOT_DIR = path.resolve(__dirname)
const BASE_DIR = path.join(ROOT_DIR, 'src')

module.exports = {
  mode: 'universal',
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  srcDir: './src',
  loading: { color: '#4b0082' },
  buildModules: [
    '@nuxtjs/vuetify',
    '@nuxtjs/moment',
  ],
  modules: [
    '@nuxtjs/toast',
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
  ],
  moment: {
    locales: ['ru'],
    defaultLocale: 'ru',
  },
  axios: {
    baseURL: 'http://localhost:8000/api/',
  },
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          info: colors.teal.lighten1,
          error: colors.deepOrange.accent4,
          accent: colors.grey.darken3,
          warning: colors.amber.base,
          success: colors.green.accent3,
          primary: colors.blue.darken2,
          secondary: colors.amber.darken3,
        }
      }
    }
  },
  build: {
    extend(config) {
      config.resolve.alias['@layouts'] = path.join(BASE_DIR, 'layouts')
      config.resolve.alias['@components'] = path.join(BASE_DIR, 'components')
    }
  }
}
