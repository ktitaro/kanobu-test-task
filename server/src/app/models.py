from django.db import models
from django.utils.timezone import now
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation


class AbstractBaseModel(models.Model):
    """
    Extends base model with fields that helps track
    the time of creation and the latest update.
    """
    created_at = models.DateTimeField(default=now, editable=False)
    updated_at = models.DateTimeField(default=now, editable=False)

    class Meta:
        abstract = True
        ordering = ('-created_at', '-updated_at')

    def save(self, *args, **kwargs):
        self.updated_at = now()
        super(AbstractBaseModel, self).save(*args, **kwargs)


class AbstractContentModel(AbstractBaseModel):
    """
    Extends base abstract model with reference to reaction instance.
    """
    author = models.CharField(max_length=100)
    content = models.TextField()
    reactions = GenericRelation('Reaction')

    class Meta:
        abstract = True


class Article(AbstractContentModel):
    """
    Represents a single article of any type.
    """
    JUST_ARTICLE = 'A'
    NEWS_ARTICLE = 'B'

    ARTICLE_TYPES = (
        (JUST_ARTICLE, 'Just an article'),
        (NEWS_ARTICLE, 'A news article'),
    )
    
    type = models.CharField(max_length=1, choices=ARTICLE_TYPES)
    title = models.CharField(max_length=100)
    pub_date = models.DateTimeField(default=now)

    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = 'Articles'


class Comment(AbstractContentModel):
    """
    Represents a single comment.
    """
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='comments')

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'


class Reaction(AbstractBaseModel):
    """
    Represents a single reaction to any content model.
    """
    POSITIVE = '+'
    NEGATIVE = '-'

    REACTION_TYPES = (
        (POSITIVE, 'Positive'),
        (NEGATIVE, 'Negative'),
    )

    uuid = models.UUIDField()
    type = models.CharField(max_length=1, choices=REACTION_TYPES)
    resource = GenericForeignKey('content_type', 'object_id')
    object_id = models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Reaction'
        verbose_name_plural = 'Reactions'