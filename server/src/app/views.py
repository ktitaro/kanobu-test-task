from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny

from django.utils.timezone import now
from django.contrib.contenttypes.models import ContentType

from .models import *
from .serializers import *


class ArticleViewSet(ModelViewSet):
    """
    An API entrypoint for the `Article`.
    """
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    permission_classes = (AllowAny,)

    @action(detail=False, methods=['get'])
    def published(self, request):
        queryset = self.get_queryset()
        queryset = queryset.filter(pub_date__lt=now()).order_by('-pub_date')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class CommentViewSet(ModelViewSet):
    """
    An API entrypoint for the `Comment`.
    """
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    permission_classes = (AllowAny,)

    @action(detail=False, methods=['get'])
    def for_article(self, request):
        pk = request.GET.get('pk')
        queryset = self.get_queryset()
        queryset = queryset.filter(article_id=pk)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ReactionViewSet(ModelViewSet):
    """
    An API entrypoint for the `Reaction`.
    """
    queryset = Reaction.objects.all()
    serializer_class = ReactionSerializer
    permission_classes = (AllowAny,)

    def perform_create(self, serializer):
        """
        Creates `Reaction` for particular resource.
        """
        pk = self.request.data.get('pk')
        model = self.request.data.get('model')
        resource_ct = ContentType.objects.get(model=model)
        resource = resource_ct.get_object_for_this_type(pk=pk)
        serializer.save(resource=resource)

    @action(detail=False, methods=['get'])
    def for_resource(self, request):
        pk = request.GET.get('pk')
        model = request.GET.get('model')

        queryset = self.get_queryset()
        queryset = queryset.filter(object_id=pk, 
            content_type=ContentType.objects.get(model=model))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=['get'])
    def for_resource_by_uuid(self, request):
        pk = request.GET.get('pk')
        uuid = request.GET.get('uuid')
        model = request.GET.get('model')

        queryset = self.get_queryset()
        queryset = queryset.filter(uuid=uuid, object_id=pk,
            content_type=ContentType.objects.get(model=model))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)