from rest_framework import serializers

from .models import *


class ReactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Reaction
        fields = ('id', 'uuid', 'type')


class CommentSerializer(serializers.ModelSerializer):
    reactions = ReactionSerializer(many=True, read_only=True)

    class Meta:
        model = Comment
        fields = ('id', 'article', 'author', 'content', 'reactions')


class ArticleSerializer(serializers.ModelSerializer):
    reactions = ReactionSerializer(many=True, read_only=True)

    class Meta:
        model = Article
        fields = ('id', 'author', 'type', 'title', 
            'content', 'pub_date', 'created_at', 'reactions')