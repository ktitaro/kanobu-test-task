from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from .models import *


class ReactionInline(GenericTabularInline):
    model = Reaction


@admin.register(Reaction)
class Reaction(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('uuid', 'type'),
        }),
        ('Resource', {
            'fields': ('content_type', 'object_id'),
        }),
    )
    list_display = ('uuid', 'type')


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('title', 'content'),
        }),
        (None, {
            'fields': ('author', 'type',  'pub_date'),
        }),
    )
    inlines = (ReactionInline,)
    list_display = ('author', 'type', 'title')


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': ('article', 'author', 'content',),
        }),
    )
    inlines = (ReactionInline,)
    list_display = ('author', 'content',)